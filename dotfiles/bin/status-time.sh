#!/bin/sh

while :
do
    DATE=$(date "+%a %y-%m-%d  %R  ")
    #NP=$(mpc current)
    NP="$(playerctl --player mpv metadata artist) - $(playerctl --player mpv metadata title)"
    VPN=$(mullvad status)
    echo -n "$NP    | ${VPN::1} | $DATE"
    sleep 5
done
