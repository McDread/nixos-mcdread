{
  description = "McDread's personal Nixos configuration flake.";
  
  /*
  nixConfig = {
    extra-experimental-features = "nix-command flakes ca-reference";
    extra-substituters = "https://nix-community.cachix.org";
    extra-trusted-public-keys = "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=";
  };
  */

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixos-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    nur.url = "github:nix-community/NUR";

    musnix.url = "github:musnix/musnix"; # realtime audio in nixos, (for rocksmith?) jack

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    hyprland.url = "github:hyprwm/Hyprland";
  };

  outputs = inputs@{ nixpkgs, nixos-stable, nur, home-manager, musnix, hyprland, ... }: 
  let
    overlay-pkgs = final: prev: {
      stable = nixos-stable.legacyPackages.${prev.system};
    };
  in {
    nixosConfigurations = {
      nixos-desktop = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ 
	  ({ config, pkgs, ... }: { nixpkgs.overlays = [ overlay-pkgs ]; })

          ./modules
          ./hardware-configuration.nix
          ./desktop_configuration.nix
          #(import ./udev.nix)

          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.mcdread = import ./home.nix;
          #  # Optionally, use home-manager.extraSpecialArgs to pass
          #  # arguments to home.nix
          }

          #inputs.hyprland.nixosModules.default 
          #{ programs.hyprland.enable = true; }

          #musnix.nixosModules.musnix
        ];
        #specialArgs = { inherit inputs; }; # used in musnix
      };

      nixos-laptop = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ 
          (import ./hardware-configuration.nix)
          (import ./laptop_configuration.nix)
          (import ./nvidia_prime.nix)
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.mcdread = import ./home.nix;
          #  # Optionally, use home-manager.extraSpecialArgs to pass
          #  # arguments to home.nix
          }
        ];
      };

      nixos-desktop-old = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ 
          (import ./hardware-configuration.nix)
          (import ./old_desktop_configuration.nix)
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.mcdread = import ./home.nix;
          #  # Optionally, use home-manager.extraSpecialArgs to pass
          #  # arguments to home.nix
          }
        ];
      };

      nixos = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ 
          (import ./hardware-configuration.nix)
          (import ./old_laptop_configuration.nix)
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.mcdread = import ./home.nix;
          #  # Optionally, use home-manager.extraSpecialArgs to pass
          #  # arguments to home.nix
          }
        ];
      };
    };
  };
}
