return {
{
  'https://github.com/nvim-orgmode/orgmode',
  dependencies = {
    { 'nvim-treesitter/nvim-treesitter', lazy = true },
  },
  --event = 'VeryLazy',
  config = function()
    -- Load treesitter grammar for org
--    require('orgmode').setup_ts_grammar() -- comment after message saying it's no longer required

    -- Setup treesitter
    require('nvim-treesitter.configs').setup({
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = { 'org' },
      },
      ensure_installed = { 'org' },
    })

    -- Setup orgmode
    require('orgmode').setup({
      org_agenda_files = '~/org/**/*',
      org_default_notes_file = '~/org/refile.org',
      org_startup_indented = true,
    })
  end,
}
}
