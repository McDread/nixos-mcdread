#!/usr/bin/env sh

PLAYLISTS="artists online radio $(find ~/music/playlists -maxdepth 1 -type f)" # -printf "%f\n")"

CHOICE="$(echo $PLAYLISTS | tr ' ' '\n' | bemenu -i -l "10 down" --fn 'mononoki Nerd Font Mono 19')"

if [ "$CHOICE" = "artists" ]
then
    ARTIST="$(find -L ~/music -type d | bemenu -i -l "10 down" --fn 'mononoki Nerd Font Mono 19')"
    find -L "$ARTIST" -type f | shuf > ~/.cache/playlist-current.m3u
    CHOICE=~/.cache/playlist-current.m3u
fi

if [ "$CHOICE" = "online" ]
then
    PLAYLISTS="$(find ~/music/playlists/online -maxdepth 1 -type f)" 
    CHOICE="$(echo $PLAYLISTS | tr ' ' '\n' | bemenu -i -l "10 down" --fn 'mononoki Nerd Font Mono 19')"
fi

if [ "$CHOICE" = "radio" ]
then
    PLAYLISTS="$(find ~/music/playlists/radio -maxdepth 1 -type f)" 
    CHOICE="$(echo $PLAYLISTS | tr ' ' '\n' | bemenu -i -l "10 down" --fn 'mononoki Nerd Font Mono 19')"
fi


if [ "$CHOICE" = "" ]
then
    echo "Yeeted out, don't do anything"
else
    playerctl --player mpv stop
    mullvad-exclude kitty --title mpvmusic mpv $CHOICE --volume=35 --shuffle --no-resume-playback --ytdl-format=best & disown
    cp $CHOICE ~/.cache/playlist-current.m3u
fi


