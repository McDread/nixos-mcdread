return {
    {
        "https://github.com/nvim-treesitter/nvim-treesitter/",
        config = function()
            require("nvim-treesitter.configs").setup({ -- document parser generator tool for syntax operations
                ensure_installed = { -- installs parser when nvim is opened
                    "c", "dockerfile", "gdscript", "go", "html", "json", "lua",
                    "luadoc", "make", "markdown", "nix", "norg", "org", "poe_filter",
                    "python", "query","vim", "vimdoc", "toml",
                },
                auto_install = true, -- installs parser for filetype when they are opened
                highlight = {
                    enable = true,
                },
                incremental_selection = { -- gives keybinds for contextual selections
                    enable = true,
                    keymaps = {
                        init_selection = "<Leader>ss", -- set to `false` to disable one of the mappings
                        node_incremental = "<Leader>si",
                        scope_incremental = "<Leader>sb",
                        node_decremental = "<Leader>sd",
                    },
                },
            })
        end,
    },
}
