# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# Settings for desktop pc with nvidia gpu
#

{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
  ];
  
  networking = {
    hostName = "nixos-desktop-old"; # Flake rebuild uses this value
    interfaces.enp5s0.useDHCP = true; # "ip link show" to get a list of interfaces
  };

  nixpkgs.config.allowUnfree = true;

  #boot.kernelPackages = pkgs.linuxPackages_latest; # Bleeding edge kernel stuff?
  hardware.cpu.intel.updateMicrocode = true;
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = with pkgs; [
      mesa.drivers
      libglvnd
      libvdpau-va-gl vaapiVdpau ];
    extraPackages32 = with pkgs.pkgsi686Linux; [ mesa.drivers libva libglvnd ];
  };
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.legacy_470;

  services.xserver = {
    enable = true;
    layout = "se";
    xkbOptions = "eurosign:e";
    displayManager.startx.enable = true;
    videoDrivers = [ "nvidia" ];
  };

  #hardware.steam-hardware.enable = true; # Sets udev rules for Steam Controller, among other devices
  powerManagement.enable = false; # Desktops don't need powermanagement

}

