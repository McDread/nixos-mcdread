
config.source('config2.py')
config.load_autoconfig(False)
#c.url.searchengines['DEFAULT'] = "https://searx.be/search?q={}"
c.url.searchengines['amazon'] = "https://www.amazon.se/s?k={}"
c.url.searchengines['alpine'] = "https://pkgs.alpinelinux.org/packages?name={}&branch=edge&repo=&arch=&maintainer="
c.url.searchengines['anydeal'] = "https://isthereanydeal.com/search/?q={}"
c.url.searchengines['arch'] = "https://wiki.archlinux.org/index.php?search={}"
c.url.searchengines['archl'] = "https://archlinux.org/packages/?sort=&q={}&maintainer=&flagged"
c.url.searchengines['ats'] = "https://hoodedhorse.com/wiki/Against_the_Storm/index.php?search={}&title=Special%3ASearch&go=Go"
c.url.searchengines['aur'] = "https://aur.archlinux.org/packages?O=0&K={}"
c.url.searchengines['bevyc'] = "https://bevy-cheatbook.github.io/tutorial/guide.html?search={}"
c.url.searchengines['bevyd'] = "https://docs.rs/bevy/latest/bevy/index.html?search={}"
c.url.searchengines['bevyi'] = "https://github.com/bevyengine/bevy/issues?q={}"
c.url.searchengines['bg3'] = "https://bg3.wiki/w/index.php?search={}&title=Special%3ASearch&fulltext=Search"
#c.url.searchengines['crate'] = "https://crates.io/search?q={}"
c.url.searchengines['cc'] = "https://openverse.org/search/?q={}&license_type=commercial,modification"
c.url.searchengines['cdd'] = "https://cdda-guide.nornagon.net/search/{}?v=0.G"
c.url.searchengines['crate'] = "https://lib.rs/search?q={}"
c.url.searchengines['crates'] = "https://crates.io/search?q={}"
c.url.searchengines['charts'] = "https://steamcharts.com/search/?q={}"
c.url.searchengines['curse'] = "https://www.curseforge.com/wow/addons/search?search={}"
c.url.searchengines['ddg'] = "https://lite.duckduckgo.com/lite/?q={}"
c.url.searchengines['df'] = "https://dwarffortresswiki.org/index.php?search={}&title=Special%3ASearch&fulltext=Search"
c.url.searchengines['dfi'] = "https://dwarffortresswiki.org/index.php/{}"
c.url.searchengines['elden'] = "https://eldenring.wiki.fextralife.com/Elden+Ring+Wiki#gsc.tab=0&gsc.q={}&gsc.sort="
c.url.searchengines['fh'] = "https://flathub.org/apps/search/{}"
c.url.searchengines['gh'] = "https://github.com/search?q={}"
c.url.searchengines['gw'] = "https://wiki.guildwars2.com/index.php?title=Special%3ASearch&search={}&go=Go&ns0=1"
c.url.searchengines['gopkg'] = "https://pkg.go.dev/search?q={}"
c.url.searchengines['godo'] = "https://docs.godotengine.org/en/stable/search.html?q={}&check_keywords=yes&area=default"
c.url.searchengines['hoogle'] = "https://hoogle.haskell.org/?hoogle={}"
c.url.searchengines['howlong'] = "https://howlongtobeat.com/?q={}"
c.url.searchengines['imdb'] = "https://www.imdb.com/find/?q={}&ref_=nv_sr_sm"
c.url.searchengines['ip'] = "https://whatismyipaddress.com/ip/{}"
c.url.searchengines['ipfs'] = "https://ipfs-search.com/#/search?q={}"
c.url.searchengines['itch'] = "https://itch.io/search?q={}"
c.url.searchengines['kingmaker'] = "https://pathfinderkingmaker.fandom.com/wiki/Special:Search?query={}&scope=internal&navigationSearch=true"
c.url.searchengines['quote'] = "https://libquotes.com/search/?q={}"
c.url.searchengines['lemmy'] = "https://lemmy.ml/search/q/{}/type/All/sort/TopAll/listing_type/All/community_id/0/creator_id/0/page/1"
c.url.searchengines['lexica'] = "https://lexica.art/?q={}"
c.url.searchengines['lutris'] = "https://lutris.net/games?q={}"
c.url.searchengines['lutrisf'] = "https://forums.lutris.net/search?q={}"
c.url.searchengines['map'] = "https://www.openstreetmap.org/search?query={}"
c.url.searchengines['melpa'] = "https://melpa.org/#/?q={}"
c.url.searchengines['music'] = "https://musicbrainz.org/search?query={}&type=artist&method=indexed"
c.url.searchengines['mynix'] = "https://mynixos.com/search?q={}"
c.url.searchengines['nb'] = "https://northboot.xyz//search?q={}"
c.url.searchengines['nms'] = "https://nomanssky.fandom.com/wiki/Special:Search?query={}"
c.url.searchengines['nix'] = "https://search.nixos.org/packages?channel=unstable&query={}"
c.url.searchengines['nixpkgs'] = "https://github.com/NixOS/nixpkgs/issues?q={}"
c.url.searchengines['nixwiki'] = "https://nixos.wiki/index.php?search={}&go=Go"
c.url.searchengines['nw'] = "https://neverwinter.fandom.com/wiki/Special:Search?fulltext=1&query={}&scope=internal&contentType=&ns%5B0%5D=0&ns%5B1%5D=2900"
c.url.searchengines['odysee'] = "https://odysee.com/$/search?q={}"
c.url.searchengines['oni'] = "https://oxygennotincluded.fandom.com/wiki/Special:Search?fulltext=1&query={}"
c.url.searchengines['onig'] = "https://oxygen-not-included-guide.fandom.com/wiki/Special:Search?query={}"
c.url.searchengines['pcgaming'] = "https://www.pcgamingwiki.com/w/index.php?search={}&title=Special%3ASearch"
c.url.searchengines['phil'] = "https://plato.stanford.edu/search/searcher.py?query={}"
c.url.searchengines['poe'] = "https://www.poewiki.net/w/index.php?search={}"
c.url.searchengines['price'] = "https://www.pricerunner.se/results?q={}"
c.url.searchengines['proton'] = "https://www.protondb.com/search?q={}"
c.url.searchengines['repo'] = "https://repology.org/projects/?search={}&maintainer=&category=&inrepo=&notinrepo=&repos=&families=&repos_newest=&families_newest="
c.url.searchengines['rust-'] = "https://doc.rust-lang.org/stable/std/index.html?search={}"
c.url.searchengines['rustb'] = "https://doc.rust-lang.org/book/title-page.html?search={}"
c.url.searchengines['satisf'] = "https://satisfactory.wiki.gg/index.php?search={}&title=Special%3ASearch&fulltext=Search"
c.url.searchengines['shroom'] = "https://www.shroomery.org/forums/dosearch.php?forum%5B%5D=f2&words={}&namebox=&replybox=&how=all&where=body&tosearch=both&newerval=5&newertype=y&olderval=&oldertype=y&minwords=&maxwords=&limit=25&sort=r&way=d"
c.url.searchengines['sp'] = "https://www.startpage.com/sp/search?query={}"
c.url.searchengines['sx'] = "https://srchng.xyz/search?q={}"
c.url.searchengines['steam'] = "https://store.steampowered.com/search/?term={}"
c.url.searchengines['temu'] = "https://www.temu.com/search_result.html?search_key={}&search_method=user&refer_page_el_sn=200010&srch_enter_source=top_search_entrance_10012&_x_enter_scene_type=cate_tab&_x_sessn_id=qnv8sk1010&refer_page_name=category&refer_page_id=10012_1701896496862_5p6tfpk5ad&refer_page_sn=10012"
c.url.searchengines['vector'] = "https://publicdomainvectors.org/en/search/{}"
c.url.searchengines['void'] = "https://voidlinux.org/packages/?arch=x86_64&q={}"
c.url.searchengines['voiddoc'] = "https://docs.voidlinux.org/?search={}"
c.url.searchengines['tyda'] = "https://tyda.se/search/{}"
c.url.searchengines['wiki'] = "https://en.wikipedia.org/w/index.php?search={}"
c.url.searchengines['wine'] = "https://www.winehq.org/search?q={}"
c.url.searchengines['wishlist'] = "https://store.steampowered.com/wishlist/profiles/76561198032241168/#sort=order&term={}"
c.url.searchengines['word'] = "https://www.wordnik.com/words/{}"
c.url.searchengines['wowhead'] = "https://www.wowhead.com/search?q={}"
c.url.searchengines['you'] = "https://you.com/search?q={}&tbm=youchat&cfr=chat"
c.url.searchengines['yt'] = "https://piped.video/results?search_query={}"
c.url.searchengines['xda'] = "https://forum.xda-developers.com/search/53191467/?q={}&o=relevance"

config.bind('<Ctrl-Shift-V>', 'spawn mpv {url}')
config.bind('<Ctrl-v>', 'hint links spawn mpv {hint-url}')

config.set("fonts.default_size", "16pt")
config.set("zoom.default", "150%")
config.set("fonts.web.size.default", 22)
config.set("colors.webpage.darkmode.enabled", True)
config.set("colors.webpage.bg", "black")
config.set("content.user_stylesheets", ["~/.config/qutebrowser/css/black.css"])
#config.set("content.image_smoothing", "false")
config.bind('<Ctrl-d>', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/darculized-all-sites.css ~/.config/qutebrowser/css/black.css')
config.bind('<Ctrl-Shift-D>', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/empty.css ~/.config/qutebrowser/css/black.css')

config.set("auto_save.session", True)
config.set("content.notifications.enabled", False)
config.set("content.register_protocol_handler", False)
config.set("content.blocking.method", "both")
c.content.blocking.adblock.lists = ['https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt', 'https://easylist-downloads.adblockplus.org/easylistdutch.txt', 'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt', 'https://www.i-dont-care-about-cookies.eu/abp/', 'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']


config.bind( # should remove cookie popups
    'ec', 'jseval (function () { ' +
    '  var i, elements = document.querySelectorAll("body *");' + '' +
    '  for (i = 0; i < elements.length; i++) {' +
    '    var pos = getComputedStyle(elements[i]).position;' +
    '    if (pos === "fixed" || pos == "sticky") {' +
    '      elements[i].parentNode.removeChild(elements[i]);' + '    }' +
    '  }' + '})();')

config.set("content.javascript.enabled", False) # tsh for enabling
# tSh to get url in "autoconfig.yml"
js_allow = [
    '*://127.0.0.1:*', 
    '*://localhost:*',
    '*://archive.org/*',
    '*://www.amazon.se/*',
    '*://web.archive.org//*',
    '*://archive.org//*',
    '*://bevy-cheatbook.github.io/*',
    '*://bg3.wiki/*',
    '*://cdda-guide.nornagon.net/*',
    '*://codeberg.org/*',
    '*://www.epicgames.com/*',
    '*://store.epicgames.com/*',
    '*://gaming.amazon.com/*',
    'https://www.fantasynamegenerators.com/*'
    '*://flathub.org/*',
    '*://docs.godotengine.org/*',
    '*://itch.io/*',
    '*://ipfs-search.com/*',
    '*://howlongtobeat.com/*',
    '*://search.nixos.org/*',
    '*://lexica.art/*',
    '*://northboot.xyz/*',
    '*://mail.proton.me/*',
    '*://melpa.org/*',
    '*://mynixos.com/*',
    '*://opencritic.com/*',
    '*://opencritic.com/*',
    '*://mail.proton.me/*',
    '*://www.reddit.com/*',
    '*://old.reddit.com/*',
    'https://www.startpage.com/*'
    '*://store.steampowered.com/*',
    '*://steamdb.info/*'
    '*://supernote.eu/*',
    '*://www.gog.com/*',
    '*://www.openstreetmap.org/*',
    'https://www.protondb.com/*',
    'https://docs.qmk.fm/*'
    '*://twitch.tv/*',
    '*://id.twitch.tv/oauth2/authorize?client_id=g5zg0400k4vhrx2g6xi4hgveruamlv*',
    '*://dashboard.twitch.tv/*'
    ]
for i in js_allow:
    config.set('content.javascript.enabled', True, i)

#config.source('redirects.py')

