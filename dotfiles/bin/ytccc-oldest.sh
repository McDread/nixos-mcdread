#!/bin/sh

if [ -z "$1" ]
then
INFO=$(ytcc -t no list -a "publish_date,id,playlists,title,url" | sort | sed -n '2p')
ID=$(echo "$INFO" | cut -d " " -f 6)
else
INFO=$(ytcc -t no list -a "publish_date,id,playlists,title,url" | grep -i "$1" | sort | sed -n '1p')
ID=$(echo "$INFO" | cut -d " " -f 6)
fi

if [ "$ID" = ""  ]
then
   echo Nothin found. Better watch something else.
else
   ytcc play $ID 
   sleep 1
   echo "$INFO" 
fi 
