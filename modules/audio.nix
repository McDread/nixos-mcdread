
{ config, lib, pkgs, ... }:

with lib; {
    imports = [];
  options.modules.services.pipewire = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Whether to enable PipeWire
      '';
    };
  };

  config = mkIf config.modules.services.pipewire.enable {

    environment.systemPackages = with pkgs; [
      easyeffects # Audio effects for PipeWire applications
      qpwgraph # Qt graph manager for PipeWire, similar to QjackCtl
    ];

    security.rtkit.enable = lib.mkDefault true; # RealtimeKit is optional but recommended for pipewire (better latency?)
    services.pipewire.enable = lib.mkDefault true; # Server and user space API to deal with multimedia pipelines
    services.pipewire.alsa.enable = lib.mkDefault true; # Whether to enable ALSA support
    services.pipewire.alsa.support32Bit = lib.mkDefault true; # Whether to enable 32-bit ALSA support on 64-bit systems
    services.pipewire.audio.enable = lib.mkDefault true; # Whether to use PipeWire as the primary sound server
    services.pipewire.pulse.enable = lib.mkDefault true; # Whether to enable PulseAudio server emulation
    services.pipewire.jack.enable = lib.mkDefault true; # Whether to enable JACK audio emulation
    services.pipewire.wireplumber.enable = lib.mkDefault true; # Whether to enable Wireplumber, a modular session / policy manager for PipeWire

    systemd.user.services.pipewire-pulse.serviceConfig.LimitMEMLOCK = lib.mkDefault "131072"; #don't know what this is

    services.pipewire.wireplumber.configPackages = [
    (pkgs.writeTextDir "share/wireplumber/bluetooth.lua.d/51-bluez-config.lua" ''
     bluez_monitor.properties = {
     ["bluez5.enable-sbc-xq"] = true,
     ["bluez5.enable-msbc"] = true,
     ["bluez5.enable-hw-volume"] = true,
     ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]",
     ["bluez5.codecs"] = "[ sbc_xq ]",
     ["bluez5.auto-connect"]  = "[ a2dp_sink ]"
     }
     '')
    ];

    # IN THE FUTURE THIS: (according to wiki)
    #services.pipewire.wireplumber.extraLuaConfig.bluetooth."51-bluez-config" = ''
    #        bluez_monitor.properties = {
    #            ["bluez5.enable-sbc-xq"] = true,
    #            ["bluez5.enable-msbc"] = true,
    #            ["bluez5.enable-hw-volume"] = true,
    #            ["bluez5.codecs"] = "[ sbc_xq ]",
    #            ["bluez5.headset-roles"] = "[  ]",
    #            ["bluez5.auto-connect"]  = "[ a2dp_sink ]"
    #        }
    #      '';
  };
}
