#!/usr/bin/env sh
#

if [ "$1" = "" ];
then
    SONG="$(find -L ~/music/* -type f -name "*.mp3" -o -name "*.m4a" -o -name "*.flac" -o -name "*.wma" -o -name "*.opus" | bemenu -i -l "10 down" --fn 'mononoki Nerd Font Mono 19')"
else
    SONG=$1
fi

if [[ $SONG == "/home/mcdread/"* || $SONG == "http"* ]];
then
    #echo "adding uri to playlist-urgent.m3u"
    echo $SONG >> ~/.cache/playlist-prio.m3u
else
    #echo "finding url to song"
    URL=$(mullvad-exclude spotdl url "$SONG" | grep http)
    echo $URL >> ~/.cache/playlist-prio.m3u
fi
