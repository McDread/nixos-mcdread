# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# Settings for desktop pc with nvidia gpu
#

{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
  ];
  
  networking = {
    hostName = "nixos"; # Flake rebuild uses this value
    networkmanager.enable = true;
    #interfaces.enp5s0.useDHCP = true; # "ip link show" to get a list of interfaces
  };

  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
  };

  #boot.kernelPackages = pkgs.linuxPackages_latest; # Bleeding edge kernel stuff?
  hardware.cpu.intel.updateMicrocode = true;

  services.xserver = {
    enable = true;
    layout = "se";
    xkbOptions = "eurosign:e";
    displayManager.startx.enable = true;
  };

  environment.systemPackages = with pkgs; [
    dwl
    bemenu
    #hyprpaper
    sway
    (wrapOBS.override { obs-studio = pkgs.obs-studio; } {
      plugins = with obs-studio-plugins; [
        wlrobs
      ];
    })
  ];

  programs.xwayland.enable = true;

  services.printing.enable = true;
  services.avahi.enable = true;
  services.avahi.nssmdns = true;
  # for a WiFi printer
  services.avahi.openFirewall = true;

  services.ipp-usb.enable = true; #scanner usb chord
  hardware.sane = {
    enable = true; #scanner driver
    extraBackends = [ pkgs.hplipWithPlugin ]; #scanner usb chord

  };
  #hardware.steam-hardware.enable = true; # Sets udev rules for Steam Controller, among other devices
}


