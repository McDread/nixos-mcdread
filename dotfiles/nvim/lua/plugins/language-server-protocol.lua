return {
    {
        "https://github.com/neovim/nvim-lspconfig", -- language server protocol (lsp) config helper
        config = function()
            require('lspconfig').gdscript.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
            })
            require('lspconfig').gopls.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
            })
            require('lspconfig').lua_ls.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
                on_init = function(client) -- pasted a bunch of settings to make it work with neovim
                    local path = client.workspace_folders[1].name
                    if not vim.loop.fs_stat(path..'/.luarc.json') and not vim.loop.fs_stat(path..'/.luarc.jsonc') then
                        client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
                            Lua = {
                                runtime = {
                                    -- Tell the language server which version of Lua you're using
                                    -- (most likely LuaJIT in the case of Neovim)
                                    version = 'LuaJIT'
                                },
                                -- Make the server aware of Neovim runtime files
                                workspace = {
                                    checkThirdParty = false,
                                    library = {
                                        vim.env.VIMRUNTIME
                                        -- "${3rd}/luv/library"
                                        -- "${3rd}/busted/library",
                                    }
                                    -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                                    -- library = vim.api.nvim_get_runtime_file("", true)
                                }
                            }
                        })
                        client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
                    end
                    return true
                end
            })
            require('lspconfig').nixd.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
            })
            require("lspconfig").pyright.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
            })
            require('lspconfig').rust_analyzer.setup({
                require("cmp_nvim_lsp").default_capabilities(), -- autocomplete
            })
        end,
    },
}
