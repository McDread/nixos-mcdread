# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

  let
    my-python-packages = ps: with ps; [
      # list packages to be made availible systemwide
      huggingface-hub # all things AI 
      requests # http requests and more
      pyserial # needed for qmk compiling
      pillow # needed for qmk compiling
    ];
  in
{

  nixpkgs.config.allowUnfree = true; # propietary software? gross
  time.timeZone = "Europe/Amsterdam";
  time.hardwareClockInLocalTime = true; # discord is reading hwclock, so better have it not be UTC (didn't work...)

  # services.ratbagd.enable = true; # Needed for piper, logitech mouse config
  #hardware.keyboard.qmk.enable = true; # need this for flashing qmk keyboard

  #TODO test adb stuff before removing
  #services.udev.extraRules = ''
  #  # Android Boot loader interface
  #  SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", MODE="0660", GROUP="adbusers", TAG+="uaccess"
  #  SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", ATTR{idProduct}=="685d", SYMLINK+="android_adb"
  #  SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", ATTR{idProduct}=="685d", SYMLINK+="android_fastboot"
  #'';

  hardware.steam-hardware.enable = true; # udev rules for gamepads 
  
  ## TODO test controllers before removing
  # services.udev.extraRules = ''
  #   # DualShock 4 over USB hidraw  
  #   KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", MODE="0660", TAG+="uaccess"  
  #   # DualShock 4 wireless adapter over USB hidraw  
  #   KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ba0", MODE="0660", TAG+="uaccess"  
  #   # DualShock 4 over bluetooth hidraw  
  #   KERNEL=="hidraw*", KERNELS=="*054C:05C4*", MODE="0660", TAG+="uaccess"  
  # '';


  environment.systemPackages = with pkgs; [
    btop # htop on steroids? monitor computer resources
    calc # cli Calculator
    curl # download webpages among other things
    dash # shell for executing shell scripts with #!/usr/bin/env dash
    dust # du and df replacement with nice tables hard drive space monitor
    eza # replacement for ls, lists files with nice info
    file # file info
    fzf # fuzzy finder
    git # version management
    gnumake # tool to control generation of non-source files from src (binaries)
    #gnupg # gpg encryption cli, used for pass
    gpg-tui # tui for gnupg
    # htop-vim # system resources monitor
    iftop # display bandwidth usage on a network interface
    jq # cli JSON processor
    keyd # change keyboard with daemon, similar to qmk (package installed for manpages etc (nix module for settings))
    linux-firmware # make stuff work good?
    lm_sensors # computer sensors (temperature)
    lynx # terminal web browser
    mullvad # vpn
    nixd # language server protocol (lsp) for nix
    nmap # a utility for network discovery and security auditing
    nodejs # javascript tool (used in some coding stuff)
    # open-dyslexic # font with increased readability (nerdfont was old and does not have the mono)
    p7zip # better archiving than tarballs?
    pass-wayland # self-maintained password-manager
    pciutils # ...?
    pinentry # enter password to terminal (needed for password manager)
    podman-compose # drop-in replacement for docker-compose (define and run containers)
    powertop # monitor battery theives 
    (python3.withPackages my-python-packages) # programming language
    #rbw # bitwarden client
    ripgrep # reimplementation of grep, "rg"
    #stable.rtorrent # torrents #broke on  old laptop for some reason
    rtorrent # torrents, for downloading linux isos
    #stable.jesec-rtorrent # rtorrents fork
    smartmontools # monitor hard drives
    tcpdump # network inspector (similar to wireshark)
    tldr # summary man pages, manual, help
    #tmux # split terminal interface to windows
    tree # view directory as tree format
    unzip # unzip zip files
    usbutils # lsusb checks usb connections
    whois # look up IP, domains
    wget # download web stuff
    xdg-utils # opening default programs when clicking links (mimeapps)
    zellij # terminal tiling "window" manager, alternative to tmux
  ];

  programs.neovim.enable = true;
  programs.neovim.defaultEditor = true;
  programs.neovim.viAlias = true;
  programs.neovim.vimAlias = true;

  #programs.nix-ld.enable = true; # Allows the running of random binaries from the internet, What could go wrong?

  networking = {
    networkmanager.enable = true; # "nmtui" for easy internet setup
    #nameservers = [ "208.67.222.222" ];  #"9.9.9.9" "94.140.14.14" # if you don't want the auto-detecteded through DHCP.
    resolvconf.dnsExtensionMechanism = false; # random recommend worked for laptop slow internet
    useDHCP = false; # deprecated, therefore set to false
  };

  services.mullvad-vpn.enable = true;
  services.openssh.enable = true;
  services.flatpak.enable = true;

    modules.services.pipewire.enable = true; # local module for audio
    modules.hardware.bluetooth.enable = true; # local module for audio

  services.printing.enable = true; # enables printing support via the CUPS daemon
  #services.avahi.enable = true; # runs the Avahi daemon, used for printers on network?
  #services.avahi.nssmdns = true; # enables the mDNS NSS plug-in
  #services.avahi.openFirewall = true; # opens the firewall for UDP port 5353

  xdg.portal.enable = true; # needed for flatpaks
  xdg.portal.extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
  xdg.portal.config.common.default = "*"; #https://github.com/flatpak/xdg-desktop-portal/blob/1.18.1/doc/portals.conf.rst.in
  #virtualisation.docker.enable = true;
  #virtualisation.libvirtd.enable = true; # virtual machines (use virt-manager)
  #programs.virt-manager.enable = true; # virtual machines (use libvirtd)

  virtualisation.podman = {
    enable = true; # run rootless containers
    dockerCompat = true; # drop-in replacement for docker
    defaultNetwork.settings.dns_enabled = true; # required for podman-compose containers to talk to eachother
  };

  xdg.mime.enable = true; # default apps for things, didn't work in home-manager #TODO mimeo instead
  # For available mimeapps run (check $XDG_DATA_DIRS) the *.desktop file should have availible MimeTypes:
  # "ls /run/current-system/sw/share/applications" (nixos)
  # "ls /etc/profiles/per-user/mcdread/share/applications" (home-manager)
  xdg.mime.defaultApplications = {
    "text/html" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/http" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/https" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/about" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/unknown" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "application/pdf" = [ "zathura.desktop" "mupdf.desktop" ];
    "image/png" = [ "imv.desktop" "feh.desktop" ];
    "image/jpeg" = [ "imv.desktop" "feh.desktop" ];
    "image/jwebp" = [ "imv.desktop" "feh.desktop" ];
    "image/gif" = [ "imv.desktop" "feh.desktop" ];
  };

  programs.adb.enable = true; # manage android device
  services.udev.packages = [
    pkgs.android-udev-rules
  ];

  programs.gnupg.agent.enable = true; # gpg, used for password-store
  programs.gnupg.agent.pinentryPackage = pkgs.pinentry-curses; # terminal password entry
  

  nix.extraOptions = ''
    experimental-features = nix-command flakes
    '';

  nix.settings = {
    trusted-users = [ "root" "@wheel" ];
    substituters = ["https://hyprland.cachix.org"]; # hyprland provides their own binaries
    trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="]; # vaxry does not seem sus.
  };

  fonts.packages = with pkgs; [
    #(nerdfonts.override { fonts = [ "DaddyTimeMono" "FantasqueSansMono" "Monofur" "Mononoki" "OpenDyslexic" ]; })
    (nerdfonts.override { fonts = [ "FiraCode" "Mononoki" "OpenDyslexic" ]; })
  ];

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "mononoki Nerd Font Mono";
    # font = "OpenDyslexic";
    keyMap = "sv-latin1";
  };

  environment = {
    variables = { EDITOR = "nvim"; };
    homeBinInPath = true; # ~/bin in $PATH
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      ls = "ls -lh --color=auto";
    };
    shellInit = ''
      #autoload -Uz vcs_info
      #precmd () { vcs_info }
      #zstyle ':vcs_info:' formats '(%b)'
      #$vcs_info_msg_0_
      PS1='%F{blue}[%F{red}%T%F{blue}]{¤F{green}%1~%F{blue}%f %F{yellow}%B$ '
    '';
    autosuggestions.enable = true;
    autosuggestions.async = false;
    syntaxHighlighting.enable = true;
  };


  #programs.starship = { # Prompt for all shells (bash, zsh, nushell)
  #  enable = true;
  #  settings = {
  #      # add_newline = false;
  #      character = {
  #        success_symbol = "[➜](bold green)";
  #        error_symbol = "[➜](bold red)";
  #      };
  #  };
  #};

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    defaultUserShell = pkgs.zsh; # the B in bash is for BLOAT
    users.mcdread = {
      isNormalUser = true; # I'm normal, you're the strange one
      initialPassword = "swordfish"; # can't login without password, best have one for fresh install
      extraGroups = [ "adbusers" "audio" "docker" "input" "jackaudio" "keyd" "libvirtd" "lp" "networkmanager" "plugdev" "realtime" "scanner" "wheel" ];
    };
  };

  services.getty.autologinUser = "mcdread"; # Who wants to spend time logging in?

  services.keyd = { # rebind keyboard buttons to other buttons
    enable = true;
    keyboards.default.settings = {
      main = {
        capslock = "layer(capslayer)";
        #menu = "down";
        compose = "down";
      };
      capslayer = {
        backspace = "delete";
        h = "left";
        j = "down";
        k = "up";
        l = "right";

        a = "left";
        s = "down";
        w = "up";
        d = "right";

        "1" = "f11"; "2" = "f12"; "3" = "f13"; "4" = "f14"; "5" = "f15"; "6" = "f16"; "7" = "f17"; "8" = "f18"; "9" = "f19"; "0" = "f20";
      };
    };
    keyboards.default.extraConfig = ''
`=escape
'';
  };

  services.disnix.enableMultiUser = false; # I'm the only user on this machine
  services.earlyoom.enable = true; # Enable early out of memory killing.
  services.earlyoom.freeMemThreshold = 3;
  services.fstrim.enable = true; # Management service for SSDs

  #programs.steam.enable = true; we be using flatpaks
  # TODO lutris esync does NOT work atm. look into it
  security.pam.loginLimits = [
    # Probably not necessary to have it quite so high but it avoids running into problems with other apps like wine.
    # See: https://github.com/lutris/docs/blob/master/HowToEsync.md
    # Might be worth adding a separate config in the future to opt into a higher value
    { domain = "*"; item = "nofile"; type = "hard"; value = "1048576"; }
  ];
  systemd.extraConfig = "DefaultLimitNOFILE=1048576"; # Set limits for Esync: https://github.com/lutris/docs/blob/master/HowToEsync.md
  systemd.user.extraConfig = "DefaultLimitNOFILE=1048576"; # Set limits for esync: https://github.com/lutris/docs/blob/master/HowToEsync.md

  networking.firewall.allowedTCPPorts = [
    27036 # Steam Remote Play
    27037 # Steam Remote Play
  ];
  networking.firewall.allowedUDPPorts = [
    27031 # Steam Remote Play
    27036 # Steam Remote Play
  ];

  boot.initrd.postDeviceCommands = ''
    echo bfq > /sys/block/sda/queue/scheduler # Use bfq i/o scheduler
  '';

  boot.kernelModules = [ "tcp_bbr" "bfq" ];
  boot.kernel.sysctl = {
    "net.core.default_qdisc" = "fq";
    "net.ipv4.tcp_congestion_control" = "bbr";
    "net.ipv4.tcp_slow_start_after_idle" = "0"; # Should help to improve performance in some cases.
    "vm.max_map_count" = 2147483642; # Improvement for games using lots of mmaps (same as steam deck) 
  };
  boot.kernelParams = [
    "vga=0x034d" # 1080p 24bit framebuffer
     "noibrs" "noibpb" "nopti" "nospectre_v2" "nospectre_v1" "l1tf=off" "nospec_store_bypass_disable" "no_stf_barrier" "mds=off" "tsx=on" "tsx_async_abort=off" "mitigations=off" # I just took this from noderunner
  ];
  boot.tmp.useTmpfs = true; # /tmp using the tmpfs filesystem which stores files in RAM
  boot.kernelPackages = pkgs.linuxPackages_latest; # Don't use the LTS kernel # commented out because musnix

  networking.hosts."0.0.0.0" = [ # Block ads / tracking
    # Found with tcpdump
    "tpc.googlesyndication.com"
    "googleads.g.doubleclick.net"
    "133.111.199.185" # Japan Network Information Center
    "143.47.190.197" # Oracle Svenska AB
    # Firefox
    "location.services.mozilla.com"
    "shavar.services.mozilla.com"
    "incoming.telemetry.mozilla.org"
    "ocsp.sca1b.amazontrust.com"
    # GameAnalytics
    "api.gameanalytics.com"
    "rubick.gameanalytics.com"
    # Google
    "www.google-analytics.com"
    "ssl.google-analytics.com"
    "www.googletagmanager.com"
    "www.googletagservices.com"
    # Redshell
    "api.redshell.io"
    "treasuredata.com"
    "api.treasuredata.com"
    "in.treasuredata.com"
    # Spotify
    "apresolve.spotify.com"
    "heads4-ak.spotify.com.edgesuite.net"
    "redirector.gvt1.com"
    # Unity Engine
    "unityads.unity3d.com.edgekey.net"
    "cdn-store-icons-akamai-prd.unityads.unity3d.com.edgekey.net"
    "cdp.cloud.unity3d.com"
    "perf-events.cloud.unity3d.com"
    "api.uca.cloud.unity3d.com"
    "config.uca.cloud.unity3d.com"
    "data-optout-service.uca.cloud.unity3d.com"
    "userreporting.cloud.unity3d.com"
    "ads.prd.ie.internal.unity3d.com"
    "ads-game-configuration-master.ads.prd.ie.internal.unity3d.com"
    "publisher-event.ads.prd.ie.internal.unity3d.com"
    "ads-privacy-api.prd.mz.internal.unity3d.com"
    "tracking.prd.mz.internal.unity3d.com"
    "stats.unity3d.com"
    "unityads.unity3d.com"
    "ads-brand-postback.unityads.unity3d.com"
    "ads-game-configuration.unityads.unity3d.com"
    "adserver.unityads.unity3d.com"
    "adsx.unityads.unity3d.com"
    "nginx-auction-prd-gcp.adsx.unityads.unity3d.com"
    "admediator.unityads.unity3d.com"
    "auction.unityads.unity3d.com"
    "auction-load.unityads.unity3d.com"
    "auiopt.unityads.unity3d.com"
    "cdn.unityads.unity3d.com"
    "cdn-creatives-highwinds-prd.unityads.unity3d.com"
    "cdn-webview-pge.unityads.unity3d.com"
    "config.unityads.unity3d.com"
    "dsp-tracking.unityads.unity3d.com"
    "dsp-vast.unityads.unity3d.com"
    "geocdn.unityads.unity3d.com"
    "httpkafka.unityads.unity3d.com"
    "pge.unityads.unity3d.com"
    "publisher-config.unityads.unity3d.com"
    "publisher-event.unityads.unity3d.com"
    "thind.unityads.unity3d.com"
    "webview.unityads.unity3d.com"
    # Unreal Engine 4
    "tracking.epicgames.com"
    "tracking.unrealengine.com"
    # Misc
    "alb.reddit.com"
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
