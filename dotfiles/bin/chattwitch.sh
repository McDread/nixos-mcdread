#!/usr/bin/env sh
# opens chat for streams opened with wtwitch
mullvad-exclude chatterino -c $(ps -efw | rg -o "title Watching \w+" | cut -d " " -f 3 | tr '\n' ';') & disown

