# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

# Settings for desktop pc with AMD gpu

{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
    # ~/stuff/musnix # cloned from https://github.com/musnix/musnix -- use flake instead
  ];

  #musnix = {
  #  enable = true;  # low latency (real-time) audio for guitar and midi on nixos use for jack
  #  soundcardPciId = "00:1f.3"; #used to set pci latency timer (get from lspci | rg Audio)
  #  #kernel.realtime = true; # WARNING rebuilds kernel default is false
  #  #kernel.packages = pkgs.linuxPackages_latest_rt; # Uses a more up-to-date (bleeding?) kernel
  #  alsaSeq.enable = true;

  #  # magic to me
  #  rtirq = { # requres realtime kernel
  #    # highList = "snd_hrtimer";
  #    resetAll = 1;
  #    prioLow = 0;
  #    enable = true;
  #    nameList = "rtc0 snd";
  #  };
  #};

  networking = {
    hostName = "nixos-desktop"; # rebuilding flake nixos uses hostname
    interfaces.enp6s0.useDHCP = true; # "ip link show" to get a list of interfaces to maybe enable
  };

  environment.systemPackages = with pkgs; [
    btrfs-progs # utility for manage file system partition
    #TODO move to home-manager (maybe don't need wlrobs anymore)
    (wrapOBS.override { obs-studio = pkgs.obs-studio; } {
      plugins = with obs-studio-plugins; [
        wlrobs
      ];
    })
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.consoleMode = "max"; # Console resolution
    systemd-boot.enable = true;
    systemd-boot.configurationLimit = 42; # maximum generations in boot menu (prevent boot partition being full)
    efi.canTouchEfiVariables = true;
    efi.efiSysMountPoint = "/boot/efi";
    timeout = 1; # Set timeout to 1 for faster boot speeds.
  };

  nixpkgs.overlays = [
    (self: super: {
      dwl = super.dwl.overrideAttrs (oldAttrs: rec {
        enable-xwayland = true;
        patches = [
          # autostart patch
          (super.fetchpatch {
            url = "https://github.com/djpohly/dwl/compare/main...dm1tz:04-autostart.patch";
            sha256 = "IuWsAeQIOkvM9a9Pjdw+iI8GUbP3vi9m7oxdhuT6YVY=";
          })
          # sway pointer constraints
          (super.fetchpatch {
            url = "https://github.com/djpohly/dwl/compare/main...sevz17:pointer-constraints.patch";
            sha256 = "ufiSdzMWRdCYkF+lceq4wJPd0HkJiDVczK9Iz4+uSC8=";
          })
          # swallow patch (hide terminal when opening window)
          (super.fetchpatch {
            url = "https://github.com/djpohly/dwl/compare/main...dm1tz:04-swallow.patch";
            sha256 = "2AvfodwD4nyv6hnTZ9Gn72zoa7LMwt6HzhW9ziP6cMk=";
          })
       ];
        configFile = super.writeText "config.h" (builtins.readFile ./dotfiles/dwl-config.h);
        postPatch = oldAttrs.postPatch or "" + "\necho 'Using own config file...'\n cp ${configFile} config.def.h";
      });
    })
  ];


  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; }; # hardware acceleration
  };

  # Explicitly set some things for gaming with amd gpu
  boot.initrd.kernelModules = [ "amdgpu" ]; # kernel load the driver right away
  hardware.cpu.intel.updateMicrocode = true;
  hardware.opengl = {
    enable = true;
    driSupport = true; # enable vulkan
    driSupport32Bit = true; # enable vulkan for 32bit applications (games likely)
    #TODO vaapi? vdpau? do I need extrapackages?

    # TODO disable all extrapackages if craches continue
    extraPackages = with pkgs; [
      mesa.drivers
      #amdvlk # try after craches and GSOD, removed again after guild wars not starting
      ## hardware acceleration
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
    extraPackages32 = with pkgs; [
      #driversi686Linux.amdvlk # try after craches adn GSOD, removed again after guild wars not starting
    ];
  };

  #hardware.bluetooth.enable = true; # get rid of them wires for sound

  programs.xwayland.enable = true; # needed for wayland compositors (sway) #TODO use home-manager module?

  services.xserver = {
    enable = true;
    xkb.layout = "se";
    xkb.options = "eurosign:e";
    displayManager.startx.enable = true;
    desktopManager.plasma5.enable = true; # try out for guild wars blish addon testing
    videoDrivers = [ "amdgpu" ];
  };

  #hardware.steam-hardware.enable = true; # Sets udev rules for Steam Controller, among other devices

  powerManagement.enable = false; # Desktops don't need powermanagement

}

