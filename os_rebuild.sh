#!/usr/bin/env sh
SCRIPT_PATH=$(dirname "$0")
warn=$(tput bold && tput setaf 0 && tput setab 7)
normal=$(tput sgr0)

echo "${warn}OPERATING SYSTEM REBUILD IMMINENT:${normal} Are you sure you want to?"
read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'

# Generate "hardware-configuration.nix" with: nixos-generate-config, place in ./secrets/
echo "         cp /etc/nixos/hardware-configuration.nix ${SCRIPT_PATH}"
sudo cp "/etc/nixos/hardware-configuration.nix" "${SCRIPT_PATH}"
echo "         git add ."
git add .

# Important to note that the flake rebuilds based on hostname
if [ "$1" = "boot" ]; 
then
   echo "         sudo nixos-rebuild boot --flake SCRIPT_PATH#hostname"
   mullvad-exclude sudo nixos-rebuild boot --flake "${SCRIPT_PATH}#${hostname}" --impure
else
   echo "         sudo nixos-rebuild switch --flake SCRIPT_PATH#hostname"
   #sudo nixos-rebuild --install-bootloader switch --flake "${SCRIPT_PATH}#${hostname}" --impure
   mullvad-exclude sudo nixos-rebuild switch --flake "${SCRIPT_PATH}#${hostname}" --impure
fi

# We don't want "hardware-configuraton.nix" in the git repo
echo "         rm hardware-configuration.nix"
rm -f ~/stuff/nixos-mcdread/hardware-configuration.nix
