#!/usr/bin/env sh
FOCUSED=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true) | .pid')
WD_FILE=$WD_CACHE_DIR/$FOCUSED
if [ -e $WD_FILE ]
then
  exec foot -D $(< $WD_FILE)
else
  exec run-cwd.sh foot
fi
