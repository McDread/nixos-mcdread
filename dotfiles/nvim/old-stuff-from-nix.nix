
    #TODO fix this neovim mess
    (neovim.override {
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [ 
            coc-nvim
            coc-rust-analyzer
            coc-python
            indentLine
            vim-lastplace 
            vim-nix 
            #rust-vim #do :CoCInstall coc-rust-analyzer
            vim-hexokinase
            nvim-fzf 
            nvim-autopairs  
            #nvim-cmp
            #cmp-buffer
            #cmp-nvim-lsp
            yuck-vim
          ]; 
          opt = [];
        };
        customRC = ''
          set nocompatible
          set backspace=indent,eol,start
          set number ruler
          set hlsearch
          set tabstop=3 shiftwidth=3 expandtab
          set clipboard=unnamedplus
          set termguicolors
          let g:Hexokinase_highlighters = [ 'backgroundfull' ]

          " use <tab> to trigger completion and navigate to the next complete item
          function! CheckBackspace() abort
            let col = col('.') - 1
            return !col || getline('.')[col - 1]  =~# '\s'
          endfunction
          inoremap <silent><expr> <Tab>
                \ coc#pum#visible() ? coc#pum#next(1) :
                \ CheckBackspace() ? "\<Tab>" :
                \ coc#refresh()
          inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

          " Make <CR> to accept selected completion item or notify coc.nvim to format
          " <C-g>u breaks current undo, please make your own choice.
          inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

          " cr to select and confirm completion
          "inoremap <expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<CR>"
          "inoremap <silent><expr> <CR> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
          "inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
          "inoremap <silent><expr> <CR> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
          "inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

          lua << EOF
            --local cmp = require'cmp'
            --cmp.setup {
            --  -- Enable LSP snippets
            --  snippet = {
            --    expand = function(args)
            --        vim.fn["vsnip#anonymous"](args.body)
            --    end,
            --  },
            --  mapping = {
            --    ['<C-p>'] = cmp.mapping.select_prev_item(),
            --    ['<C-n>'] = cmp.mapping.select_next_item(),
            --    -- Add tab support
            --    ['<S-Tab>'] = cmp.mapping.select_prev_item(),
            --    ['<Tab>'] = cmp.mapping.select_next_item(),
            --    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
            --    ['<C-f>'] = cmp.mapping.scroll_docs(4),
            --    ['<C-Space>'] = cmp.mapping.complete(),
            --    ['<C-e>'] = cmp.mapping.close(),
            --    ['<CR>'] = cmp.mapping.confirm({
            --      behavior = cmp.ConfirmBehavior.Insert,
            --      select = true,
            --    })
            --  },
            --  -- Installed sources
            --  sources = {
            --    { name = 'buffer' },
            --    { name = 'nvim_lsp' },
            --    { name = 'path' },
            --    { name = 'vsnip' },
            --  },
            --}

            require('nvim-autopairs').setup()
          EOF
        '';
      };
    })
