
#c.url.searchengines['DEFAULT'] = "https://northboot.xyz//search?q={}"
c.url.searchengines['DEFAULT'] = "https://www.startpage.com/sp/search?query={}"

js_allow = [
    '*://archive.org/*',
    '*://codeberg.org/*'
    ]
for i in js_allow:
    config.set('content.javascript.enabled', True, i)

config.set("content.user_stylesheets", ["~/.config/qutebrowser/css/black.css"])
