#!/bin/sh
#

FILENAME="screenshot-`date +%F-%T`"

if [ "window" = "$1" ]
then
   grim -g "$(slurp)" ~/Downloads/$FILENAME.png
else
   grim ~/Downloads/$FILENAME.png
fi

