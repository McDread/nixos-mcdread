#!/usr/bin/env sh

running=$(ps -e | grep emacs)

if [ "$running" = ""  ]
then
    mullvad-exclude emacs --daemon
fi

mullvad-exclude emacsclient -nc
