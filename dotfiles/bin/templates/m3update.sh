#!/bin/sh

SONGS=$(find -L ~/music/* -type f -name "*.mp3" -o -name "*.m4a" -o -name "*.flac" -o -name "*.wma" -o -name "*.opus")
echo "$SONGS" | shuf > ~/music/playlists/offline.m3u

cat ~/music/playlists/offline.m3u > ~/music/playlists/all.m3u
cat ~/music/playlists/online/*.m3u | uniq >> ~/music/playlists/all.m3u
shuf ~/music/playlists/all.m3u -o ~/music/playlists/all.m3u

# cat ~/music/playlists/online/state-azure-* > ~/music/playlists/online/ambience.m3u
# cat ~/music/playlists/online/rythms-of-world.m3u >> ~/music/playlists/online/ambience.m3u
# cat ~/music/playlists/online/lofi-girl.m3u >> ~/music/playlists/ambience.m3u
# cat ~/music/playlists/online/doom-sludge.m3u >> ~/music/playlists/ambience.m3u

GAMESONGS=$(cat ~/music/playlists/offline.m3u | grep -iE "\
final fantasy\
|darren korb\
|kingdom hearts\
|outer wilds\
|zelda\
|hollow knight\
|bloodborne\
") 
echo "$GAMESONGS" > ~/music/playlists/games.m3u


PSYCHSONGS=$(cat ~/music/playlists/offline.m3u | grep -iE "\
Bathory\
|Björk\
|Detektivbyrån\
|Final Fantasy\
|Leech\
|Mastodon\
|MONO & world’s end girlfriend\
|Nine Inch Nails\
|Opeth\
|Ossler\
|Phideaux\
|Polyphia\
|Steffen Schackinger\
|Shpongle\
|Wintergatan\
|World's End Girlfriend\
|Zelda Reorchestrated\
") 
echo "$PSYCHSONGS" > ~/music/playlists/psych.m3u


AMERICANASONGS=$(cat ~/music/playlists/offline.m3u | grep -iE "\
Barns Courtney\
|Benjamin Dakota Rogers\
|Ben Miller Band\
|Bishop Gunn\
|Black Rebel Motorcycle Club\
|Blake Shelton\
|Blues Saraceno\
|Brent Cobb\
|Colby Acuff\
|Colter Wall\
|Drayton Farley\
|Drive-By Truckers\
|Goodnight, Texas\
|Ian Noe\
|Joshua Quimby\
|Justin Townes Earle\
|Kolton Moore\
|Oliver Anthony\
|Paul Cauthen\
|Ryan Bingham\
|Sturgill Simpson\
|The Dead South\
|The Devil Makes Three\
|The Ghost of Paul Revere\
|The Soggy Bottom Boys, Dan Tyminski\
|The Steeldrivers\
|The White Buffalo\
|Town Mountain\
|Tyler Childers\
|Uncle Lucius\
|Whiskey Myers\
|Zach Bryan\
") 
echo "$AMERICANASONGS" > ~/music/playlists/americana.m3u

