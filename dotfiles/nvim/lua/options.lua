
vim.opt.number = true -- show leftside line-number
vim.opt.ruler = true -- show line, column and percent on status-bar
vim.opt.cursorline = true -- highlight current line
vim.opt.cursorcolumn = true -- highlight current column
vim.opt.virtualedit = "block" -- lets you move over the entire screen (even without characters)

vim.opt.hlsearch = true -- highlight search results
vim.opt.ignorecase = true -- treat upper and lower case the same
vim.opt.smartcase = true -- improves ignorecase 

vim.opt.expandtab = true -- uses spaces instead of tab symbol
vim.opt.tabstop = 4 -- how many spaces to a tab when saving
vim.opt.shiftwidth = 4 -- how many spaces when >> indent << dedent

vim.opt.foldmethod = "marker" -- Fold code so I don't have to look at it all
--vim.opt.matchpairs = "<:>,(:),{:},[:],\":\",\':\'" -- all the pairs to match.
vim.opt.matchpairs = "<:>,(:),{:},[:]" -- all the pairs to match.
-- vim.opt.highlight MatchParen ctermbg=blue guibg=lightblue

vim.opt.clipboard = "unnamedplus" -- sync clipboards, copy and paste from other applications

vim.opt.splitbelow = true -- open new window under current
vim.opt.splitright = true -- window to the right if vertical vsplit

vim.opt.scrolloff = 99 -- minimum lines to keep above, below cursor

vim.opt.termguicolors = true -- If I wanted a gui I would have used emacs, now it uses the terminal's colors LIES!!!
--vim.cmd.colorscheme("tokyonight")

vim.g.mapleader = " " -- spacevim rather than spacemacs, my own keybinds starts with leader
