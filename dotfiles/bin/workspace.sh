#!/bin/sh

STATUS="$(swaymsg -t get_workspaces --raw | jq --arg ws $1 'map(select(.name == $ws)).[0]' | jq '.focused')"
if [ "$STATUS" == "true" ]
then
    swaymsg workspace "$1$1"
else
    swaymsg workspace "$1"
fi


