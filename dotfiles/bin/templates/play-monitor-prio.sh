#!/usr/bin/env sh
#
PRIO=~/.cache/playlist-prio.m3u
CURR=~/.cache/playlist-current.m3u

while true;
do
    if [ "$(tail $PRIO -n 1)" = "" ]
    then
        echo "No prio list, TODO logic to play current?"
        #playerctl --player mpv status
        #if [ "$?" = "1" ];
        #then
        #    echo "No player, starting current playlist"
        #    mullvad-exclude kitty --title mpvmusic mpv $CURR --volume=35 --ytdl-format=best & disown
        #fi
    else
        playerctl --player mpv status
        if [ "$?" = "1" ];
        then
            echo "No player, starting prio playlist"
            mullvad-exclude kitty --title mpvmusic mpv $PRIO --volume=35 --no-resume-playback --ytdl-format=best & disown
            sleep 1
            sed -i '1d' $PRIO
        else
            echo "Already playing, waiting to start prio"
            LEFT=$(playerctl metadata --format '{{mpris:length - position}}')
            if [ 2050000 -gt $LEFT ];
            then 
                playerctl --player mpv stop
            fi
        fi
    fi
sleep 2
done



