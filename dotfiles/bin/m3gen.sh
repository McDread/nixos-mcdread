#!/usr/bin/env sh

## shell script to generate list of URLs from a youtube playlist

mkdir -p ~/music/playlists/online # make sure dir exists
if [ "$1" == "" ];
then
    echo "Provide youtube playlist/channel url please"
else
    PLNAME=$(yt-dlp -j --flat-playlist $1 --playlist-end 1 --playlist-reverse | jq -r .playlist)
    echo "Generating ~/music/playlists/online/$PLNAME.m3u"
    # yt-dlp -j --flat-playlist $1 --playlist-end 10 --playlist-reverse | jq -r .webpage_url >> ~/music/playlists/online/$PLNAME.m3u
    yt-dlp -j --flat-playlist $1 | jq -r .webpage_url > ~/music/playlists/online/$PLNAME.m3u
fi
