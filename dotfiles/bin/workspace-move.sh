#!/bin/sh

#MONITOR="$(swaymsg -t get_outputs | jq 'map(select(.focused == true)).[0]' | jq '.name')"
#echo $MONITOR

STATUS="$(swaymsg -t get_workspaces --raw | jq --arg ws $1 'map(select(.name == $ws)).[0]' | jq '.focused')"
if [ "$STATUS" == "true" ]
then
    swaymsg "move container to workspace number $1$1"
else
    swaymsg "move container to workspace number $1"
fi


