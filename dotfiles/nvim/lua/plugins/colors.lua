return {
    {
        "https://github.com/folke/tokyonight.nvim", --colorscheme
        lazy = false,
        --priority = 1000,
        opts = {
            style = "night",
            transparent = true,
            styles = {
                sidebars = "transparent",
                --floats = "transparent",
            },
        },
        --config = function()
        --    vim.cmd.colorscheme("tokyonight") -- set the colorscheme
        --end,
    },
    {
        "https://github.com/rktjmp/lush.nvim", -- make your own colorschemes with realtime feedback
        opts = {},
        config = function()
        end,
    },
    {
        "colorscheme-mcdread", -- my lush colorscheme
        dir = "~/.config/nvim/colorscheme-mcdread",
        opts = {},
        config = function()
            --vim.cmd.colorscheme("colorscheme-mcdread") -- transparent
        end,
    },
    {
        "https://github.com/samharju/synthweave.nvim", --colorscheme
        lazy = false, -- make sure we load this during startup if it is your main colorscheme
        priority = 1000,
        config = function()
            -- vim.cmd.colorscheme("synthweave") -- not transparent
            vim.cmd.colorscheme("synthweave-transparent") -- transparent
        end
    },
    {
        "scottmckendry/cyberdream.nvim", --colorscheme
        lazy = false,
        priority = 1000,
        config = function()
            require("cyberdream").setup({
                -- Recommended - see "Configuring" below for more config options
                transparent = true,
                italic_comments = true,
                hide_fillchars = true,
                borderless_telescope = true,
                terminal_colors = true,
                theme = {
                    highlights = {
                        CursorLine = { bg = "#222222" },
                        MatchParen = { bg = "#ff00ff" },
                    }
                },
            })
            -- vim.cmd("colorscheme cyberdream") -- set the colorscheme
        end,
    },
    {
        "https://github.com/talha-akram/noctis.nvim", --colorscheme
        lazy = true, -- false for tab-completion
        priority = 1000,
        config = function()
            --vim.cmd.colorscheme("noctis-") -- transparent
        end
    },
    {
        "https://github.com/ellisonleao/gruvbox.nvim", --colorscheme
        lazy = true, -- false for tab-completion
        priority = 1000,
        opts = {
            --background = "dark",
            transparent_mode = true,
        },
        config = function()
            --vim.cmd.colorscheme("noctis-") -- transparent
        end
    },
    {
        "https://github.com/luisiacc/gruvbox-baby", --colorscheme
        lazy = true,
        priority = 1000,
        opts = {
            --background_color = "dark",
            transparent_mode = true,
        },
        config = function()
            --vim.cmd.colorscheme("gruvbox-baby") -- transparent
        end,
    },
}
