return {
    {
        "https://github.com/windwp/nvim-autopairs/", -- automatically braces {} brackets [] parenthesis ()
        opts = {}, -- this is equalent to setup({}) function 
        event = "InsertEnter", -- Loads when entering insert mode
    },
    {
        'https://github.com/numToStr/Comment.nvim', -- comment or uncomment lines with "gcc" or "[count]gcc"
        opts = {
            -- add any options here
        },
        lazy = false,
    },
    {
        "https://github.com/RRethy/vim-hexokinase",  -- show colors #ff0000 rgb(0,255,0) blue
        init = function() -- do before load
            vim.g.Hexokinase_highlighters = { 'backgroundfull', } -- set how to show color
        end,
        build = "make hexokinase", -- do on install/update
        config = function() -- do after load
            vim.opt.termguicolors = true -- must be true for the plugin to work
        end,
    },
    {
        "https://github.com/lukas-reineke/indent-blankline.nvim", -- show indent lines 
        main = "ibl",
        opts = {},
        config = function() -- do after load
            require('ibl').setup()
        end
    },
    {
        "https://github.com/lewis6991/foldsigns.nvim", -- show lsp errors on folds
        config = function()
            require('foldsigns').setup()
        end
    },
    {
        "https://github.com/ethanholz/nvim-lastplace", -- opens files where last closed
        opts = {} -- this is equalent to setup({}) function 
    },
    {
        "https://github.com/rareitems/hl_match_area.nvim", -- highlights area between matchpairs (i.e. {}()[]<>)
        opts = {} -- this is equalent to setup({}) function 
    },
    -- {
    --     "https://gitlab.com/HiPhish/rainbow-delimiters.nvim",
    --     -- opts = {} -- this is equalent to setup({}) function 
    -- },
}
