#!/usr/bin/env sh

export NIXPKGS_ALLOW_UNFREE=1

if [ "-u" = "$1" ] # true if any argument passed
then
   mullvad-exclude nix shell --impure "nixpkgs/nixos-unstable#$2"
else
   mullvad-exclude nix shell --impure "nixpkgs/nixos-24.05#$1"
fi


