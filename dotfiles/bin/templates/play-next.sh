#!/usr/bin/env sh
#
PRIO=~/.cache/playlist-prio.m3u

if [ "$(tail $PRIO -n 1)" = "" ]
then
    playerctl --player mpv next
else
    echo "starting playlist-prio.m3u"
    playerctl --player mpv stop
    mullvad-exclude kitty --title mpvmusic mpv $PRIO --volume=35 --no-resume-playback --ytdl-format=best & disown
    sleep 1
    sed -i '1d' $PRIO
fi




