#!/usr/bin/env sh
for i in *.mkv;
do 
    ffmpeg -i "$i" -vcodec libx265 -crf 28 "new/${i%.*}.mkv";
done
