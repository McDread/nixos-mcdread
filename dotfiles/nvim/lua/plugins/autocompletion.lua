-- remember to add autocomplete capabilities to language server protocol (lsp)
return {
    {
        "https://github.com/hrsh7th/cmp-nvim-lsp/" -- lsp source for nvim-cmp
    },
    {
        "https://github.com/hrsh7th/cmp-buffer/" -- buffer source for nvim-cmp
    },
    {
        "https://github.com/L3MON4D3/LuaSnip" -- snippets, for nvim-cmp
    },
    {
        "https://github.com/saadparwaiz1/cmp_luasnip" -- luasnip completion source for nvim-cmp
    },
    {
        "https://github.com/hrsh7th/nvim-cmp/", -- autocompletion plugin
        config = function() -- this whole thin taken from https://github.com/neovim/nvim-lspconfig/wiki/Autocompletion
            local luasnip = require 'luasnip'
            local cmp = require 'cmp'
            cmp.setup {
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end,
                },
                mapping = cmp.mapping.preset.insert({
                    ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
                    ['<C-d>'] = cmp.mapping.scroll_docs(4), -- Down
                    -- C-b (back) C-f (forward) for snippet placeholder navigation.
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ['<CR>'] = cmp.mapping.confirm {
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = true,
                    },
                    ['<Tab>'] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        elseif luasnip.expand_or_jumpable() then
                            luasnip.expand_or_jump()
                        else
                            fallback()
                        end
                    end, { 'i', 's' }),
                    ['<S-Tab>'] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        elseif luasnip.jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end, { 'i', 's' }),
                }),
                sources = {
                    { name = 'buffer' },
                    { name = 'luasnip' },
                    { name = 'nvim_lsp' },
                    { name = 'orgmode' },
                },
            }
        end,
    },
}
