# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# Config for laptop with Nvidia Optimus (hybrid) gpu. 
# Not currently setup for gaming (except remote play ofc)
#

{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
  ];

  environment.systemPackages = with pkgs; [
    #epr
    #virt-manager
    polychromatic
    razergenie
    xorg.xmodmap
  ];

  # Used for virt-manager
  #virtualisation.libvirtd.enable = true;
  virtualisation.docker.enable = true;
  #programs.dconf.enable = true;

  hardware.openrazer = {
    enable = true;
    users = [ "mcdread" ];
  };
  
  networking = {
    hostName = "nixos-laptop"; # Flake rebuild uses this value
    interfaces.enp9s0.useDHCP = true; # "ip link show" to get a list of interfaces
    interfaces.wlp8s0.useDHCP = true;
  };

  # Enable the X11 windowing system. Transition this to home-manager
  services.xserver = {
    enable = true;
    layout = "se";
    xkbOptions = "eurosign:e";
    displayManager.startx.enable = true;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.consoleMode = "max"; # Console resolution
    systemd-boot.enable = true;
    systemd-boot.configurationLimit = 42; # maximum generations in boot menu (prevent boot partition being full)
    efi.canTouchEfiVariables = true;
    #efi.efiSysMountPoint = "/boot/efi";
    timeout = 1; # Set timeout to 1 for faster boot speeds.
  };

  # https://nixos.wiki/wiki/Printing
  services.printing.enable = true; # enables printing support via the CUPS daemon
  services.avahi.enable = true; # runs the Avahi daemon (printing)
  services.avahi.nssmdns = true; # enables the mDNS NSS plug-in
  services.avahi.openFirewall = true; # opens the firewall for UDP port 5353

  # https://nixos.wiki/wiki/Scanners 
  services.ipp-usb.enable = true; #scanner usb chord
  hardware.sane.enable = true; # scanner driver # remember to set "scanner" and "lp" to USER.extraGroups
  hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ]; # scanner usb chord

}


