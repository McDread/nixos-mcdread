## https://nixos.wiki/wiki/Nvidia#offload_mode
#
# # nix-shell -p lshw --run "lshw -c display"|grep "bus info"
#       bus info: pci@0000:01:00.0
#       bus info: pci@0000:06:00.0
#
{ config, pkgs, ... }:
let
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    export DRI_PRIME=1
    exec -a "$0" "$@"
  '';
  whichgpu = pkgs.writeShellScriptBin "whichgpu" ''glxinfo | grep vendor'';
  nvidiaon = pkgs.writeShellScriptBin "nvidiaon" ''
    export __NV_PRIME_RENDER_OFFLOAD=1;
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0;
    export __GLX_VENDOR_LIBRARY_NAME=nvidia;
    export __VK_LAYER_NV_optimus=NVIDIA_only;
    glxinfo | grep vendor; echo OK!;
  '';
in
  {  
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [ nvidia-offload whichgpu nvidiaon glxinfo ];
  hardware.opengl.enable = true;
  hardware.opengl.extraPackages = with pkgs; [ 
    mesa.drivers 
    libglvnd
    intel-media-driver 
    libvdpau-va-gl vaapiVdpau vaapiIntel ];
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ mesa.drivers libva libglvnd vaapiIntel ];

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  
  services.xserver = {
    enable = true;
    layout = "se";
    xkbOptions = "eurosign:e";
    displayManager.startx.enable = true;
    videoDrivers = [ "nvidia" ];
  };

  hardware.nvidia.prime = {
    offload.enable = true;
    #sync.enable = true;

    # Bus ID of the AMD GPU. You can find it using lspci, either under 3D or VGA
    intelBusId = "PCI:0:2:0";

    # Bus ID of the NVIDIA GPU. You can find it using lspci, either under 3D or VGA
    nvidiaBusId = "PCI:1:0:0";
  };
}
