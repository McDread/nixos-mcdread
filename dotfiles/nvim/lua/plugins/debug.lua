return {
    {
        "https://github.com/mfussenegger/nvim-dap",
        config = function()
        end,
        opts = {}, -- this should be equivalent to setup({}) function
    },
    {
        "https://github.com/leoluz/nvim-dap-go", -- depends on "delve" being installed
        config = function()
            require('dap-go').setup()
        end,
        opts = {},
    },

}
