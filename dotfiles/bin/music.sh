#!/bin/sh

while :
do
   if [ -z "$1" ] || [ "-a" = "$1" ]
   then
      ID=$(ytcc -t no list -a "publish_date,id,playlists,title" | grep Music | shuf | sed -n '2p' | cut -d " " -f 6)
   else
      ID=$(ytcc -t no list -a "publish_date,id,playlists,title" | grep Music | grep $1 | shuf | sed -n '1p' | cut -d " " -f 6)
   fi

   if [ "-a" = "$1" ]
   then
      ytcc play -a $ID
   else
      ytcc play $ID
   fi
   echo "played id: $ID"
   sleep 1
done

