{ config, pkgs, ... }:

{
  home.username = "mcdread"; # what user are we managing for?
  home.homeDirectory = "/home/mcdread"; # where is your home?
  home.sessionPath = [ "$HOME/bin" ]; # list directories to add to $PATH

  nixpkgs.config.allowUnfree = true; # difficult to play games without this

  home.packages = with pkgs; [
    android-tools # adb (transfer files/install shit etc)
    #apkeep # Download APK for android
    arandr # simple visual frontend for xrandr (set up monitors / screens)
    #ardour # digital audio workstation, making and playing music
    (aspellWithDicts (dicts: with dicts; [ en en-computers en-science sv]))
    #aspell # spell checking (for emacs)
    #aspellDicts.en # english dictionary for aspell (there are additions to science and tech available)
    #aspellDicts.sv # swedish dictionary for aspell
    bat # better cat, with highlighting and everything
    #beets # music organization and tagging 
    bemenu # wayland launcher/menu
    # cargo # rust development
    # cargo-info # show crate info from crates.io for rust programming
    #carla # audio plugin host (for midi controller)
    chatterino2 # twitch chat
    # delve # debugger for go golang programming language (install nvim-dap and nvim-dap-go)
    distrobox # wrapper for podman to run containers that are integrated with the host
    #dmenu # xorg menu/launcher
    dotool # xdotool for everywhere. simulate keyboard and mouse input commands (check out "libei" if not happy)
    easyeffects # used to mess with audio
    entr # run command when a file changes. AUTOMATE EVERYTHING!
    #eww-wayland # desktop widgets
    feh # xorg image viewer, setting wallpaper on i3
    fluidsynth # software music synthesizer play audio from midi controller
    ffmpeg-full # audio / video manipulation
    foot # terminal emulator
    freetube # gui for youtube # get from flatpak?
    gamescope # SteamOS session compositing wm, use it for mouse lock / constraint issues
    #gargoyle # interactive fiction reader https://ifdb.org/viewgame?id=aearuuxv83plclpl
    gcc # gnu c compiler
    gitui # help with git commands through tui
    #gnuplot # cli graph plotter
    go # programming language
    # gopls # language server protocol (lsp) for go
    gomuks # terminal, tui, matrix client for chatting 
    grim # grab images (screenshots) wayland (use with slurp)
    handlr-regex # Open file types, replacement for mime, xdg_open
    #helix # nvim / kak inspired editor
    #heroic # game launcher for epic, gog and amazon (use flatpak instead)
    #howl # lightweight text-editor inspired by emacs and vim
    #html2text # extract text from html tags
    #hunspell # spell checker
    i3 # xorg window manager
    #inkscape # vector graphics editor
    imv # image viewer for wayland
    #itch # games store/launcher (indie)
    #kakoune # text editor
    kitty # terminal emulator
    legendary-gl # interface to the epic games store # use heroic instead
    librewolf # firefox fork web browser
    lingot # not only a guitarr tuner
    #lgogdownloader # interface to the gog game store # use heroic instead
    lua-language-server # language server protocol (lsp) for lua
    lutris # game installer / launcher
    #maptool # ttrpg interface virtual tabletop
    mimeo # what app opens what file with regex (e.g. youtube links with mpv)
    #mono # used to install unity mod manager
    mpvc # mpc style interface for mpv # TODO fix music shit
    #musescore # transcribe music, play midi
    #mupdf # pdf viewer
    neofetch # print system info summary
    #nix-init # generate nix packages from URLs
    nodePackages_latest.pyright # language server protocol (lsp) for python
    nushell # modern shell (rust) with structured data instead of strings
    nvtopPackages.amd # monitor gpu usage
    #nyxt # lisp web browser, flatpak is recommended 
    piper # mouse manager # used with services.ratbagd
    playerctl # used to interface with playerctld #TODO fix music shit
    ponymix # cli for pulseaudio
    #postgresql # object-relational database system (consul maybe)
    projectm # audio visualizer
    #protontricks # for fixing steam game installs
    pspg # postgres tui, also for browsing other sql
    pulsemixer # audio (volume) control
    python311Packages.adblock # adblock needed for qutebrowser
    #pyradio # terminal radio player, #TODO broke last build
    qjackctl # controller for jack sound server daemon (audio setup)
    #qmk # program for flashing keyboard layout
    qutebrowser # web browser with vim style keybindings
    qpwgraph # pipewire graph setup (with saving)
    qsynth # fluidsynth gui, for midi controller
    rocmPackages.rocm-smi # gpu system management interface, used in btop
    #ruby # programming language, object oriented bs (consul)
    #rubyPackages.rails # make web apps in ruby (consul)
    # rustc # programming language
    # rust-analyzer # language server protocol (lsp) for rust
    # rustfmt # formatter for rust
    sc-im # spreadsheet, cell, csv editor with vim style keybinds, libxls security issues
    #steam # games store/launcher (indie)
    #steamtinkerlaunch # games custom launches
    slurp # select region in wayland (screenshot util)
    #sonic-pi # program to make music with code # some issues, used flatpak instead
    spotdl # download spotify playlists #TODO not installing, failed to build
    sqlite # self contained transactional database
    # steam-run # used for running binaries #maybe get when needed since it's slowing down updates by building
    streamlink # watch streams (twitch, youtube)
    streamlink-twitch-gui-bin # gui to twitch
    sway # wayland compositor i3-compatible
    swaybg # wallpaper manager for sway
    syncthing # for syncing files between devices, webui: http://127.0.0.1:8384/
    #termusic # terminal music player/downloader in rust
    timer # Visual terminal timer.
    #vcv-rack #Open-source virtual modular synthesizer for music making #don't keep it always, takes forever to build
    #vimiv-qt # gui image viewer vim-style
    #vlc # videolan media player, used for mpris integration
    warpd # A modal keyboard driven interface for mouse manipulation. (movement vim normal mode ) #TODO build with wayland?
    #wbg # simple wallpaper for wayland
    weechat # irc chat client (and more)
    #wego # weather cli
    wf-recorder # wlroots screen-recording (works with slurp)
    #wineWowPackages.stable # wine with support for both 32- and 64-bit apps
    wineWowPackages.waylandFull # wine with support for both 32- and 64-bit apps
    #wineWowPackages.staging # wine with support for both 32- and 64-bit apps, experimental features
    #wineasio # ASIO to JACK driver for wine (for rocksmith)
    winetricks # wine works out of the box and does not need tricks
    wl-clipboard # clipboard for wayland
    wlr-randr # xrandr clone for wlroots compositors, set up monitors
    #wonderdraft # ttrpg map maker
    wtwitch # tui for twitch watching #TODO fix since maintainer didn't update API key
    xclip # xorg clipboard manager
    #xdotool # automate keyboard and mouse in xorg
    #xorg.xmodmap # change keyboard keybindings in xorg
    xorg.xwininfo # get xorg information about windows (using it for chatbow atm)
    ytcc # youtube subscription manager
    zathura # document reader with vim keybinds
    zola # static site generator 

    ## games
    #astromenace # 3d space shooter game
    #cataclysm-dda # turn-based survival game
    #colobot # rts program bots game
    #crawl # dcss dungeon crawl stone soup game
    # crawlTiles # dcss dungeon crawl stone soup game
    #ddnet # teeworlds modification with coop game
    #endless-sky # sandbox space exploration game
    #freeorion # 4x galactic conquest game
    #freeciv # 4x civilization II game
    # openttd # transport tycoon game
    #openttd-jgrpp # see nixpkgs for patches / addons?
    #keeperrl # dungeon management rogue-like game
    # linthesia # music game for midi controllers with midi file
    #mari0 # 2d platformer with portals game
    #minetest # minecraft block sandbox  core game
    #mindustry-wayland # sandbox tower-defence game 
    #opendungeons # dungeon keeper strategy game 
    #openlierox # real time chaos worms combat
    #pingus # lemmings style puzzle game
    #pioneer #  space adventure game 
    #shipwright # zelda oot runner: https://ia802600.us.archive.org/view_archive.php?archive=/28/items/ship-of-harkinian/ZELOOTD.zip
    #simutrans # transport system simulation game
    #simutrans_binaries # transport system simulation game
    #starsector # space exploration game https://fractalsoftworks.com/
    #superTux # 2d platformer game
    #superTuxKart # 3d kart / cart racing game
    #teeworlds # retro multiplayer shooter game 
    # tonelib-jam # guitar practice, rocksmith rs compatible, tonelib does other guitar stuff 
    #unciv # 4x civilization 5 game
    #widelands # rts multiple goods economy game
    #wesnoth # turn-based strategy game
    #zeroad # rts game similar to age of empires
  ];

  services.playerctld.enable = true; # daemon to control media players, TODO maybe mpvc
  #services.syncthing.enable = true; # daemon to sync files between devices (can't enable cuz vpn)

  # home.pointerCursor = {
  #   name = "Black";
  #   package = pkgs.comixcursors;
  #   size = 48;
  #   gtk.enable = true;
  #   x11 = {
  #       enable = true;
  #       defaultCursor = "Black";
  #   };
  # };


  programs.emacs = {
    enable = true; # lisp interpreter (work environment, text editing etc)
    package = pkgs.emacs29-pgtk; # pure gtk 29 introduces background transparency
  };

  programs.mpv = {
    enable = true; # Pretty damn sweet media player
    scripts = [
      pkgs.mpvScripts.mpris # needed for playerctl control
      pkgs.mpvScripts.sponsorblock # required addon for mpv
    ];
  };

  programs.yt-dlp.enable = true;

programs.starship.enable = true; # Shell prompt for most shells (bash, zsh, nushell etc) ~/.config/starship.toml
programs.carapace.enable = true; # Tab auto completion for most shells (bash, zsh, nushell etc)


  #wayland.windowManager.hyprland = {
  #  enable = true;
  #  # plugins = [ # can be packages or absolute paths
  #  #];
  #  extraConfig = ''
  #    '';
  #};

  ## maintain some dotfiles
  home.file."bin".recursive = true; # create entire dir
  home.file."bin".source = ./dotfiles/bin; # create bin files
  home.file.".config".recursive = true; # create entire dir
  home.file.".config".source = ./dotfiles/config; # create bin files
  #TODO the same for config files
  #home.file.".config".recursive = true; # create entire dir
  #home.file.".config".source = ./dotfiles/bin; # create config files

  qt.enable = true; # qt configuration (themes etc)
  qt.platformTheme.name = "adwaita";
  qt.style.name = "adwaita-dark";

  #xdg.mime.enable = true; # default apps for things # HOME-MANAGER SEEMS BROKEN HERE
  # For available mimeapps run (check $XDG_DATA_DIRS) the *.desktop file should have availible MimeTypes:
  # "ls /run/current-system/sw/share/applications" (nixos)
  # "ls /etc/profiles/per-user/mcdread/share/applications" (home-manager)
  xdg.mimeApps.defaultApplications = {
    "text/html" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/http" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/https" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/about" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "x-scheme-handler/unknown" = [ "org.qutebrowser.qutebrowser.desktop" "io.gitlab.librewolf-community" "lynx.desktop" ];
    "application/pdf" = [ "zathura.desktop" "mupdf.desktop" ];
    "image/png" = [ "imv.desktop" "feh.desktop" ];
    "image/jpeg" = [ "imv.desktop" "feh.desktop" ];
    "image/jwebp" = [ "imv.desktop" "feh.desktop" ];
    "image/gif" = [ "imv.desktop" "feh.desktop" ];
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  programs.home-manager.enable = true; # Let Home Manager install and manage itself.
}
